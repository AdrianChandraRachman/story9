from django.test import TestCase, Client
from django.urls import resolve
from .views import home, register

# Create your tests here.
class Win(TestCase):

    def test_home_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_view_works(self):
        response = Client().post('/register/', data={'username':'Beidou', 'password1':'Pacil2019', 'password2':'Pacil2019'})
        self.assertRedirects(response, "/", status_code=302)
